---
layout: page
title: About
permalink: /about/
---

We are a group of Free software users and enthusiasts currently living in UAE.
Please join our [matrix chatroom](https://riot.im/app/#/room/#dfsc:matrix.org) to connect with us.


You can find the source code for this site at [gitlab](https://gitlab.com/fscuae/fscuae.gitlab.io)
